package com.test;

import com.test.controller.Controller;
import com.test.model.Comment;
import com.test.model.Post;
import com.test.model.Profile;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import retrofit2.Response;

import java.util.List;

/**
 * @author Ansari on 2/26/2019
 */

@Test
public class FakeTest {

    private static Controller controller;

    @BeforeMethod
    public void setUp(){
        controller = new Controller();
        System.out.println("Initialize the controller");
    }

    @Test(priority = 1)
    public void profileApiTest(){
        Response<List<Profile>> response = controller.getProfile();
        Assert.assertEquals(response.body().size(),2);
        System.out.println(response.body().size());
        Assert.assertTrue(response.body().get(1).getName().equalsIgnoreCase("Naved"));
        Assert.assertTrue(response.body().get(0).getName().equalsIgnoreCase("typicode"));
    }

    @Test(priority = 2)
    public void postApiTest(){
        Response<List<Post>> response = controller.getPost();
        List<Post> responseList = response.body();
        int i = responseList.get(0).getId();
        Assert.assertEquals(response.code(),200);
        Assert.assertEquals(i,1);
        Assert.assertEquals(responseList.get(0).getTitle(),"json-server");
        Assert.assertEquals(responseList.get(0).getAuthor(),"typicode" );
    }

    @Test(priority = 3)
    public void addCommentTest(){
        Comment comm = new Comment();
        comm.setId(5555); // Change the ID to avoid duplication
        comm.setCommentBody("Another comment from naved");
        comm.setPostId(1);
        Response<Comment> response = controller.sendComment(comm);
        Assert.assertEquals(response.code(),201);
        Assert.assertEquals(response.body().getCommentBody(), "Another comment from naved");
        Assert.assertEquals(response.body().getPostId(), new Integer(1));
        Assert.assertEquals(response.body().getId(), new Integer(5555));
    }

    @Test(priority = 4)
    public  void deleteExistingCommentTest(){
        int id = 5555;
        boolean flag = false;
        Response<Comment> response = controller.deleteComment(id);

        Assert.assertNotNull(response);
        //Check the id does not exist after delete
        Response<List<Post>> response1 = controller.getPost();
        List<Post> responseList = response1.body();

        for(Post p: responseList){
            System.out.println(p.getId());
            if(p.getId() == id){
                flag = true;
            }
        }
        Assert.assertFalse(flag);
        Assert.assertEquals(response.code(),200);

    }

    @Test(priority = 5)
    public  void deleteNonExistingCommentTest(){
        int id = 5555;
        Response<Comment> response = controller.deleteComment(id);
        Assert.assertNull(response.body());
        //Check the id does not exist after delete
        Assert.assertEquals(response.code(),404);

    }

    @AfterMethod
    public void tearDown(){
        controller = null;
        System.out.println("Test execution ends");
    }
}