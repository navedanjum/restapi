package com.test.serverapi;

import com.test.model.Comment;
import com.test.model.Post;
import com.test.model.Profile;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * @author Ansari on 2/26/2019
 */
public interface FakeServerAPI {

    @GET("/posts")
    Call<List<Post>> getAllPosts();

    @GET("/profile")
    Call<List<Profile>> getAllProfiles();

    @POST("/comments")
    Call<Comment> createComment(@Body Comment comment);

    @DELETE("/comments/{id}")
    Call<Comment> deleteCommentById(@Path("id") int commentId);

}
