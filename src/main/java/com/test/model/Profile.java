package com.test.model;

/**
 * @author Ansari on 2/26/2019
 */
public class Profile {
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
