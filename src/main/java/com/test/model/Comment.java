package com.test.model;

/**
 * @author Ansari on 2/26/2019
 */
public class Comment {
    Integer id;
    Integer postId;
    String commentBody;

    public Integer getId() {
        return id;
    }

    public Integer getPostId() {
        return postId;
    }

    public String getCommentBody() {
        return commentBody;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public void setCommentBody(String commentBody) {
        this.commentBody = commentBody;
    }

}
