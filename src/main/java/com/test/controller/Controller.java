package com.test.controller;

import com.test.model.Comment;
import com.test.model.Post;
import com.test.model.Profile;
import com.test.serverapi.FakeServerAPI;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.List;

/**
 * @author Ansari on 2/26/2019
 */

public class Controller  {

    static final String URL = "http://localhost:3000/";
    public static FakeServerAPI fakeServerAPI;

    public Controller()  {
        Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

        fakeServerAPI = retrofit.create(FakeServerAPI.class);

    }

    public Response<List<Profile>> getProfile(){
        Response<List<Profile>> response = null;
        try {
            response = fakeServerAPI.getAllProfiles().execute();

        }catch (IOException e){
            e.printStackTrace();
        }
        return response;
    }

    public Response<List<Post>> getPost(){
        Response<List<Post>> response = null;

        try {
            response = fakeServerAPI.getAllPosts().execute();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return response;
    }

    public  Response<Comment> sendComment(Comment comment){
        Response<Comment> response = null;

        try {
            response = fakeServerAPI.createComment(comment).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;

    }

    public Response<Comment> deleteComment(int id){

        Response<Comment> response = null;

        try {
            response = fakeServerAPI.deleteCommentById(id).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

}
