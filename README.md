# Rest-API test automation
## Description

* webservices automated tests using the java retrofit2 specification  
* Retrofit documentation: https://square.github.io/retrofit/    
* A very basic approach to utilize retrofit for web-service api automation    
* Fake api server is used as server and responses are validated for, https methods - GET/POST/DELETE responses  
* Allure reporr is used for reporting test results     
*  https://docs.qameta.io/allure/        

## Prerequisite    
* Java 8 or higher   
* Maven 3 or higher   
* Allure report   
* TestNg        

## Contact information
1. For queries contact author Navedanjum Ansari    
                              navedanjum.ansari@gmail.com
							  ansari@ut.ee / naansa@ttu.ee     
							  Personal website: xpressionworks.com